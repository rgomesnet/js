function Stack(){
    let items = [];

    // adiciona um novo item ao topo da pilha.
    this.push = function(element){
        items.push(element);
    }

    // remove o item do topo da pilha.
    this.pop = function(){
        return items.pop();
    }

    // devolve o elemento que está no topo da pilha.
    this.peek = function(){
        return items[items.length - 1];
    }

    // devolve true se a pilha não contiver nenhum elemento e false se o tamanho da pilha for maior que 0;
    this.isEmpty = function(){
        return items.length == 0;
    }

    // remove todos os elementos da pilha.
    this.clear = function(){
        item = [];
    }

    // devolve o número de elementos contidos na pilha.
    this.size = function(){
        return items.length;
    }

    this.print = function(){
        console.log(items.toString());
    }
}

function divideBy2(decNumber){
    var remStack = new Stack(),
        rem,
        binaryString = '';

    while(decNumber > 0){
        rem = Math.floor(decNumber % 2);
        remStack.push(rem);
        decNumber = Math.floor(decNumber/2);
    }

    while(!remStack.isEmpty()){
        binaryString += remStack.pop().toString();
    }

    return binaryString;
}

console.log(`divideBy2(233) | ${divideBy2(233)}`);
console.log(`divideBy2(10) | ${divideBy2(10)}`);
console.log(`divideBy2(1000) | ${divideBy2(1000)}`);