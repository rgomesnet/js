function Stack(){
    let items = [];

    // adiciona um novo item ao topo da pilha.
    this.push = function(element){
        items.push(element);
    }

    // remove o item do topo da pilha.
    this.pop = function(){
        return items.pop();
    }

    // devolve o elemento que está no topo da pilha.
    this.peek = function(){
        return items[items.length - 1];
    }

    // devolve true se a pilha não contiver nenhum elemento e false se o tamanho da pilha for maior que 0;
    this.isEmpty = function(){
        return items.length == 0;
    }

    // remove todos os elementos da pilha.
    this.clear = function(){
        item = [];
    }

    // devolve o número de elementos contidos na pilha.
    this.size = function(){
        return items.length;
    }

    this.print = function(){
        console.log(items.toString());
    }
}

function baseConverter(decNumber, base){
    var remStack = new Stack(),
        rem,
        baseString = '',
        digits = '0123456789ABCDEF';
    
    while(decNumber > 0){
        rem = Math.floor(decNumber%base);
        remStack.push(rem);
        decNumber = Math.floor(decNumber/base);
    }

    while(!remStack.isEmpty()){
        baseString += digits[remStack.pop()];
    }

    return baseString;
}

console.log(`baseConverter(100345,2) | ${baseConverter(100345,2)}`);
console.log(`baseConverter(100345,2) | ${baseConverter(100345,8)}`);
console.log(`baseConverter(100345,2) | ${baseConverter(100345,16)}`);
console.log(`baseConverter(8,16) | ${baseConverter(8,16)}`);
console.log(`baseConverter(17,16) | ${baseConverter(17,16)}`);