/* Uma pilha é uma coleção ordenada de itens que segue o princípio LIFO (Last In First Out), isto é, o último a entrar é o primeiro a sair). A adição de novos itens ou a remoção de itens ocorrem na mesma extremidade. O final da pilha é conhecido como topo, enquanto o lado oposto é conhecido como base. Os elementos mais recentes ficam próximos ao topo e os elementos mais antigos estão próximos à base. */


function Stack(){
    let items = [];

    // adiciona um novo item ao topo da pilha.
    this.push = function(element){
        items.push(element);
    }

    // remove o item do topo da pilha.
    this.pop = function(){
        return items.pop();
    }

    // devolve o elemento que está no topo da pilha.
    this.peek = function(){
        return items[items.length - 1];
    }

    // devolve true se a pilha não contiver nenhum elemento e false se o tamanho da pilha for maior que 0;
    this.isEmpty = function(){
        return items.length == 0;
    }

    // remove todos os elementos da pilha.
    this.clear = function(){
        item = [];
    }

    // devolve o número de elementos contidos na pilha.
    this.size = function(){
        return items.length;
    }

    this.print = function(){
        console.log(items.toString());
    }
}

// usando a classe Stack
console.log('let stack = new Stack();')
let stack = new Stack();

console.log(`stack.isEmpty() | ${stack.isEmpty()}`);

// adicionando elementos na pilha
console.log('stack.push(5);')
stack.push(5);

console.log('stack.push(8);')
stack.push(8);

console.log(`stack.peek() | ${stack.peek()}`);

console.log('stack.push(11);')
stack.push(11);
console.log(`stack.size() | ${stack.size()}`);
console.log(`stack.isEmpty() | ${stack.isEmpty()}`);

console.log('stack.push(15);')
stack.push(15);
// removendo elementos
console.log(`stack.pop(); | ${stack.pop()}`);
console.log(`stack.pop(); | ${stack.pop()}`);
console.log(`stack.size() | ${stack.size()}`);
console.log(`stack.peek() | ${stack.peek()}`);
stack.print();