var botaoBuscar = document.querySelector('#buscar-pacientes');

botaoBuscar.addEventListener('click', function() {
    var xhr = new XMLHttpRequest();
    var servico = 'http://demo6782210.mockable.io/pacientes';
    xhr.open('GET', servico);
    xhr.addEventListener('load', function() {

        if (xhr.status == 200) {
            removeErroAjax();
            var resposta = xhr.responseText;
            var pacientes = JSON.parse(resposta);
            pacientes.forEach(function(paciente) {
                adicionaPacienteNaTabela(paciente);
            });
        } else {
            mostraErroAjax();
        }
        // console.log(typeof resposta);
        // console.log('response - ' + xhr.response);
        // console.log('responseXML - ' + xhr.responseXML);
        // console.log('responseURL - ' + xhr.responseURL);
        // console.log('responseType - ' + xhr.responseType);
        // console.log('responseText - ' + xhr.responseText);
    });
    xhr.send();
});

function removeErroAjax() {
    document.querySelector('#erro-ajax').classList.add('invisivel');
}

function mostraErroAjax() {
    document.querySelector('#erro-ajax').classList.remove('invisivel');
}