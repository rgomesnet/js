var botaoAdicionar = document.querySelector('#adicionar-paciente');
botaoAdicionar.addEventListener('click', function(event){    
    event.preventDefault();
    var form = document.querySelector('#form-adiciona');
    var paciente = obtemPacienteDoFormulario(form);
    var pacienteTr = montaTr(paciente);    

    var erros = validaPaciente(paciente);    
    if(erros.length > 0){
        exibeMensagensDeErro(erros);        
        return;
    }

    var pacientes = document.querySelector('#tabela-pacientes');
    pacientes.appendChild(pacienteTr);
    form.reset();    
    var ul = document.querySelector('#mensagem-erro');
    ul.innerHTML = '';
});

function exibeMensagensDeErro(erros){    
    var ul = document.querySelector('#mensagem-erro');
    ul.innerHTML = '';
    erros.forEach(function(erro){
        var li = document.createElement('li');
        li.textContent = erro;
        ul.appendChild(li);
    });
}

function validaPaciente(paciente){
    var erros = [];

    if (paciente.nome.length == 0) {
        erros.push("O nome não pode ser em branco");
    }

    if (paciente.gordura.length == 0) {
        erros.push("A gordura não pode ser em branco");
    }

    if (paciente.peso.length == 0) {
        erros.push("O peso não pode ser em branco");
    }

    if (paciente.altura.length == 0) {
        erros.push("A altura não pode ser em branco");
    }

    if (!validaPeso(paciente.peso)) {
        erros.push("Peso é inválido");
    }

    if (!validaAltura(paciente.altura)) {
        erros.push("Altura é inválida");
    }
    
    return erros;
}

function obtemPacienteDoFormulario(form){
    return { 
        nome : form.nome.value,
        peso : form.peso.value,
        altura : form.altura.value,
        gordura : form.gordura.value,
        imc : calcularImc(form.peso.value, form.altura.value)
    };
}

function montaTr(paciente){
    var novoPaciente = criaTr('paciente');

    novoPaciente.appendChild(montaTd(paciente.nome,"info-nome"));
    novoPaciente.appendChild(montaTd(paciente.peso,"info-peso"));
    novoPaciente.appendChild(montaTd(paciente.altura,"info-altura"));
    novoPaciente.appendChild(montaTd(paciente.gordura,"info-gordura"));
    novoPaciente.appendChild(montaTd(paciente.imc,"info-imc"));

    return novoPaciente;
}

function montaTd(dado, classe){
    var td = document.createElement('td');
    td.textContent = dado;
    td.classList.add(classe);
    return td;
}

function criaTr(classe){
    var tr =  document.createElement('tr');
    tr.classList.add(classe);
    return tr;
}