var botaoGerar = document.querySelector('#gerar');
botaoGerar.addEventListener('click', function() {
    try {
        limparTabela();
        var quantide = obtemNumeroDeCombinacoes();
        var ms = megasena();
        var apostas = ms.create(quantide);
        mostraApostasGeradas(apostas);
    } catch (ex) {
        var mensagemErro = document.querySelector("#msgError");
        mensagemErro.innerHTML = ex.message;
    }
});

function limparTabela() {
    var tabelaCombinacoes = document.querySelector("#tabela-combinacoes");
    var parent = tabelaCombinacoes.parentElement;
    parent.removeChild(tabelaCombinacoes);
    var tabelaCombinacoes = parent.createTBody();
    tabelaCombinacoes.id = "tabela-combinacoes";
}

function obtemNumeroDeCombinacoes() {
    var quantidade = Number(document.querySelector('#quantidade').value);
    if (numeroValido(quantidade)) {
        return quantidade;
    } else {
        throw new TypeError(quantidade + ' ou vazio, não é um valor válido. Apenas números maiores do que 0 são aceitos.');
    }
}

function numeroValido(numero) {
    return !isNaN(numero) && numero >= 1;
}

function mostraApostasGeradas(apostas) {
    var tabelaCombinacoes = document.querySelector("#tabela-combinacoes");

    apostas.forEach(function(aposta, index) {
        var trAposta = document.createElement("tr");
        var tdNumero = document.createElement("td");
        tdNumero.innerHTML = index + 1;
        var tdAposta = document.createElement("td");
        tdAposta.innerHTML = aposta;
        trAposta.appendChild(tdNumero);
        trAposta.appendChild(tdAposta);
        tabelaCombinacoes.appendChild(trAposta);
    });
}