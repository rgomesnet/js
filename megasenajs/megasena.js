function megasena(){

	function aposta(){
		
		var numbers = [];

		return {
			hasNumber: function(n){
				return numbers.indexOf(n) != -1;
			},
			addNumber: function(n){
				numbers.push(n);
			},
			toString: function(){
				return numbers.sort(function(a,b){ return a-b; }).join("-");
			},
			count: function(){
				return numbers.length;
			}
		}
	};

	function apostaFactory(){
		getNumber = function(){
			return Math.floor(Math.random() * (60 - 1)) + 1;
		}

		return {
			create : function(){
				var nova = aposta();
				do{
					var number = getNumber();

					if(!nova.hasNumber(number))
						nova.addNumber(number);
				}while(nova.count() < 6);
				return nova;
			}
		}
	};

	function apostasGenerator(){
		var factory = apostaFactory();
		var apostas;

		hasAposta = function(a){
			for(var i = 0; i < apostas.length; i++)
				if(apostas[i] == a.toString()) return true;

			return false;
		}

		return{
			create: function(n){
				apostas = [];			
				do{				
					var nova = factory.create();
					if(!hasAposta(nova)){
						apostas.push(nova.toString());
					}
				}while(apostas.length < n);
				return apostas;
			}		
		}
	};

	return {
		create: function(n){
			var generator = apostasGenerator();
			return generator.create(n);
		}
	}
}